# Kode Injector

Chrome extension for JS and CSS code injection from local files to the specified websites

## How to install for developing

* `git clone`
* `make install`
* `make build`
* `Turn on developer mode in your chrome`
* `install unpacked extension from the build folder`

## TODO
- Handle popup open on the service pages
